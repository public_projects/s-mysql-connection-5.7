package org.green.datasource.db.connection.main;


import org.green.datasource.db.connection.manager.ConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ConnectDB implements CommandLineRunner {

    @Autowired
    private ConnectionManager connectionManager;

    @Override
    public void run(String... args) throws Exception {
        connectionManager.connect("testuser", "testuser", "test_db", "jdbc:mysql://localhost:3306/");
    }
}
