package org.green.datasource.db.connection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SMysqlConnectionApplication {
	public static void main(String[] args) {
		SpringApplication.run(SMysqlConnectionApplication.class, args);
	}
}
