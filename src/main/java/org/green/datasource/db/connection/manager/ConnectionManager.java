package org.green.datasource.db.connection.manager;

import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Service
public class ConnectionManager {
    public void connect(String username, String password, String database, String URL) {
        Connection conn = null;
        Statement statement = null;
        try {
            conn = DriverManager.getConnection(URL + database, username, password);
            statement = conn.createStatement();
            testQuery(database, statement);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            closeStatement(statement);
            closeConnection(conn);
        }
    }

    private static void testQuery(String database, Statement statement) throws SQLException {
        ResultSet result = statement.executeQuery("show tables;");

        while (result.next()) {
            String tableName = result.getString    ("Tables_in_" + database);
            System.err.println(tableName);
        }
    }

    private static void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
