@echo off

REM Step 1: Set JAVA_HOME environment variable
set JAVA_HOME=D:\softwares\Java\jdk-21

REM Display the updated JAVA_HOME
echo JAVA_HOME is set to: %JAVA_HOME%

REM Step 2: Run "mvn clean install"
call mvn clean install -DskipTests

REM Add a 5-second delay
timeout /t 2 /nobreak

REM Step 3: Run "mvn spring-boot:run"
call mvn spring-boot:run

REM Step 4: Pause to view the output (comment out if not needed)
pause